## Lancement

Avec docker-composer: 

```bash
docker compose up -d
```

Avec les Dockerfile séparé : 
```bash
docker build -t app .

docker network create tp1

docker run -itd -v .:/app -p 80:80 --network tp1 --restart=always  app
docker run -itd -v ./data:/data/db --network tp1 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=root --restart=always --hostname=mongo mongo:latest
```