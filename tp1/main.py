from fastapi import FastAPI
from dateutil import parser
from pymongo_get_database import get_database
from bson.objectid import ObjectId

app = FastAPI()

db = get_database()
collection = db["stocks"]


@app.get("/stocks")
async def get_stocks():
   collection = db["stocks"]
   stocks = collection.find()
   
   ret = []
   for stock in stocks:
       stock["_id"] = str(stock["_id"])
       ret.append(stock)
   return ret

@app.get("/stocks/{stock_id}")
async def get_stock(stock_id: str):
    collection = db["stocks"]
    stock = collection.find_one({"_id": ObjectId(stock_id)})
    if stock == None:
      return {"error": "Stock not found"}
    stock["_id"] = str(stock["_id"])
    return stock

@app.post("/stocks")
async def create_stock(stock: dict):
    collection = db["stocks"]
    stock_id = collection.insert_one(stock).inserted_id
    return {"_id": str(stock_id)}

@app.put("/stocks/{stock_id}")
async def update_stock(stock_id: str, stock: dict):
    collection = db["stocks"]
    collection.update_one({"_id": ObjectId(stock_id)}, {"$set": stock})
    return {"_id": stock_id}