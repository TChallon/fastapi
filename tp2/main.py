from fastapi import FastAPI
from dateutil import parser
from get_database import get_database
from bson.objectid import ObjectId

app = FastAPI()

db = get_database()

@app.get("/clients")
async def get_clients():
    cursor = db.cursor()
    cursor.execute("SELECT * FROM clients")
    ret = []
    for row in cursor.fetchall():
        ret.append({"id": row[0], "firstname": row[1], "lastname": row[2], "email": row[3], "nb_commande": row[4], "created_at": row[5]})
    return ret

@app.get("/clients/{client_id}")
async def get_client(client_id: str):
    cursor = db.cursor()
    cursor.execute("SELECT * FROM clients WHERE id = %s", (client_id,))
    ret = {}
    data = cursor.fetchone()
    if data:
        ret = {"id": data[0], "firstname": data[1], "lastname": data[2], "email": data[3], "nb_commande": data[4], "created_at": data[5]}
    return ret

@app.post("/clients")
async def create_client(client: dict):
    cursor = db.cursor()
    cursor.execute("INSERT INTO clients (firstname, lastname, email, nb_commande) VALUES (%s, %s, %s, %s)", (client["firstname"], client["lastname"], client["email"], client["nb_commande"]))
    db.commit()
    return {"_id": cursor.lastrowid}

@app.put("/clients/{client_id}")
async def update_client(client_id: str, stock: dict):
    cursor = db.cursor()
    cursor.execute("UPDATE clients SET firstname = %s, lastname = %s, email = %s, nb_commande = %s WHERE id = %s", (stock["firstname"], stock["lastname"], stock["email"], stock["nb_commande"], client_id))
    db.commit()
    return {"_id": client_id}